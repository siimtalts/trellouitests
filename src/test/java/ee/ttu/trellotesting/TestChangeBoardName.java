package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestChangeBoardName extends TestBase {

    @Test
    public void testChangeBoardName() throws Exception {
        super.login("trellouitesting@gmail.com");
        try {
            String text = driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a/span/span")).getText();
            assertEquals("Welcome board", text);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a")).click();
        driver.findElement(By.xpath("//div[@id='content']/div/div/div/a/span")).click();
        driver.findElement(By.xpath("//input[@value='Welcome board']")).clear();
        driver.findElement(By.xpath("//input[@value='Welcome board']")).sendKeys("Board 1");
        driver.findElement(By.xpath("//input[@value='Rename']")).click();
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
        try {
            assertEquals("Board 1", driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a/span/span")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a")).click();
        driver.findElement(By.xpath("//div[@id='content']/div/div/div/a/span")).click();
        driver.findElement(By.xpath("//input[@value='Board 1']")).clear();
        driver.findElement(By.xpath("//input[@value='Board 1']")).sendKeys("Welcome board");
        driver.findElement(By.xpath("//input[@value='Rename']")).click();
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}

