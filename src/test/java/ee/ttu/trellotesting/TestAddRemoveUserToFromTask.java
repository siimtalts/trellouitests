package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestAddRemoveUserToFromTask extends TestBase {

    @Test
    public void testAddRemoveUserToFromTask() throws Exception {
        super.login("trellouitesting@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[1]/div/a[1]")).click();
        driver.findElement(By.xpath("//span[@name='TrelloUiTesting4 (useruitesting4)']")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[5]/div[1]/div[1]/div/div/span[1]")).click();
        try {
            assertEquals("TrelloUiTesting4", driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/div/div/div[2]/h3/a")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        testRemoveUserFromTask();
    }


    private void testRemoveUserFromTask() {
        super.login("trellouitesting@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            //Just ignore
        }
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a[1]/div[3]/div[3]/div/span[@title=\"TrelloUiTesting4 (useruitesting4)\"]")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/div/ul/li[2]/a")).click();

        driver.findElement(By.xpath("//*[@id=\"header\"]/div[5]/a[4]")).click();
        driver.findElement(By.linkText("Log Out")).click();

        this.login("trellouitestinguser4@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();

        try {
            driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a[1]/div[3]/div[3]/div/span[@title=\"TrelloUiTesting4 (useruitesting4)\"]")).click();
            verificationErrors.append("User still linked to the task");
        } catch (Throwable e) {
            //Couldnt find user with task anymore
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
