package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class TestAddRemoveImportanceLabelToTask extends TestBase {

    @Test
    public void testAddRemoveImportanceLabelToTask() throws Exception {
        super.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();

        driver.findElement(By.xpath("//*[@id=\"board\"]/div[6]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[1]/div/a[2]")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/div/ul/li[4]/span")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();

        List<WebElement> elems = driver.findElements(By.xpath("//*[@id=\"board\"]/div[6]/div/div[2]/a/div[3]/div[1]/span"));

        try {
            assertTrue("Must contain red label tag", elems.get(0).getAttribute("class").contains("card-label-red"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        testRemoveLabel();
    }

    private void testRemoveLabel() throws Exception {
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[6]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[5]/div[1]/div[2]/div/span")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/div/ul/li[4]/span")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();

        List<WebElement> elems = driver.findElements(By.xpath("//*[@id=\"board\"]/div[6]/div/div[2]/a/div[3]/div[1]/span"));

        try {
            assertTrue("Label tags must not be present", elems.size() == 0);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
