package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestAddRemoveUserToBoard extends TestBase {

    @Test
    public void testAddRemoveUserToBoard() throws Exception {
        super.login("trellouitestinguser6@gmail.com");

        try {
            assertEquals("Welcome board", driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a/span[2]/span")).getText());
        } catch (Throwable e) {
            LOGGER.info("Board not seen");
            //Fine coz we don't want to see the board
            //verificationErrors.append(e.toString());
        }
        driver.findElement(By.xpath("//*[@id=\"header\"]/div[4]/a[4]")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitesting@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();

        try {
            driver.findElement(By.cssSelector(".board-menu.js-fill-board-menu hide"));
        } catch (Exception ex) {
            driver.findElement(By.cssSelector(".board-header-btn.mod-show-menu.js-show-sidebar")).click();
        }
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/a[1]")).click();
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).click();
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).clear();
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).sendKeys("TrelloUiTesting6");
        driver.findElement(By.xpath("//span[@name='TrelloUiTesting6 (useruitesting6)']")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[5]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitestinguser6@gmail.com");

        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[3]/span")).click();
        try {
            assertEquals("Welcome board", driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a/span[2]/span")).getText());
        } catch (Throwable e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        testRemoveUserFromProject();
    }

    private void testRemoveUserFromProject() throws Exception {
        this.login("trellouitestinguser6@gmail.com");

        try {
            assertEquals("Welcome board", driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a/span[2]/span")).getText());
        } catch (Throwable e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitesting@gmail.com");

        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a/span[2]")).click();
        driver.findElement(By.xpath("//span[@title='TrelloUiTesting6 (useruitesting6)']")).click();
        driver.findElement(By.linkText("Remove from Board…")).click();
        driver.findElement(By.xpath("//input[@value='Remove Member']")).click();
        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/div/a[2]")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[5]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitestinguser6@gmail.com");

        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        try {
            assertEquals("Welcome board", driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a/span[2]/span")).getText());
            verificationErrors.append("Welcome board should not be visible");
        } catch (Throwable e) {
            LOGGER.info("Welcome board not visible.");
            //verificationErrors.append(e.toString());
        }
    }


    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
