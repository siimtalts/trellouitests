package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TestCreateNewUser extends TestBase {

    @Test
    public void testCreateNewUser() throws Exception {
        driver.get("https://trello.com/");
        driver.findElement(By.linkText("Log In")).click();
        driver.findElement(By.id("user")).click();
        driver.findElement(By.id("user")).clear();
        driver.findElement(By.id("user")).sendKeys("trellouitestinguser3@gmail.com");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("Wyzjx6EmpN");
        driver.findElement(By.id("login")).click();

        try {
            driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/div/h3")).getText();
            //Just because for some reason next line doesn't work
            //assertEquals("There isn't an account for this email", driver.findElement(By.xpath("//div[@id=\"error\"]/p")).getText());
            LOGGER.warning("Account already defined!");
            assertFalse(true);
        } catch (Throwable e) {
            LOGGER.info("Account not defined");
            //Couldn't find element. All good. Means we didn't log in so moving on.
        }
        LOGGER.info("Creating account");
        driver.get("https://trello.com");
        driver.findElement(By.linkText("Sign Up")).click();
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).clear();
        driver.findElement(By.id("name")).sendKeys("TrelloUiTesting3");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("trellouitestinguser3@gmail.com");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("Wyzjx6EmpN");
        driver.findElement(By.id("signup")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();
        driver.findElement(By.linkText("Log In")).click();
        driver.findElement(By.id("user")).click();
        driver.findElement(By.id("user")).clear();
        driver.findElement(By.id("user")).sendKeys("trellouitestinguser3@gmail.com");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("Wyzjx6EmpN");
        driver.findElement(By.id("login")).click();
        try {
            assertEquals("Personal Boards", driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/div/h3")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}

