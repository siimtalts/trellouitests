package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertEquals;

public class TestDragAndDropTask extends TestBase {
    @Test
    public void testDragAndDrop() throws Exception {
        super.login("trellouitesting@gmail.com");
        driver.findElement(By.linkText("Welcome board")).click();

        WebElement taskToDrag = driver.findElement(By.cssSelector(".list-card.js-member-droppable"));
        WebElement firstColumn = driver.findElement(By.cssSelector("#board > .js-list.list-wrapper:nth-child(1)"));
        WebElement secondColumn = driver.findElement(By.cssSelector("#board > .js-list.list-wrapper:nth-child(2)"));

        String taskNameBeforeDragging = taskToDrag.findElement(By.cssSelector(".js-card-name")).getText();

        Actions builder = new Actions(driver);
        Action dragAndDrop = builder.clickAndHold(taskToDrag)
                .moveToElement(secondColumn)
                .release(secondColumn)
                .build();
        dragAndDrop.perform();

        taskToDrag = secondColumn.findElement(By.cssSelector(".list-cards > .list-card:last-child"));
        String taskNameAfterDragging = taskToDrag.findElement(By.cssSelector(".js-card-name")).getText();

        assertEquals(taskNameBeforeDragging, taskNameAfterDragging);

        dragAndDrop = builder.clickAndHold(taskToDrag)
                .moveToElement(firstColumn)
                .release(firstColumn)
                .build();
        dragAndDrop.perform();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
