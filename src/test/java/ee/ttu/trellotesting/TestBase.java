package ee.ttu.trellotesting;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.junit.Assert.fail;


public class TestBase {

    protected WebDriver driver;
    protected StringBuffer verificationErrors = new StringBuffer();
    final static Logger LOGGER = Logger.getLogger(TestBase.class.getName());

    @Before
    public void setUp() throws Exception {
        final String dir = System.getProperty("user.dir");
        LOGGER.info("current dir = " + dir);
        System.setProperty("webdriver.chrome.driver", dir + "/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    protected void login(String user) {
        driver.get("https://trello.com/");
        driver.findElement(By.linkText("Log In")).click();
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            LOGGER.warning("Sleep failed");
        }
        driver.findElement(By.id("user")).click();
        driver.findElement(By.id("user")).clear();
        driver.findElement(By.id("user")).sendKeys(user);
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("Wyzjx6EmpN");
        driver.findElement(By.id("login")).click();
    }

    protected void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!StringUtils.EMPTY.equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
