package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestAddJpegPngFileToTask extends TestBase {
    @Test
    public void testAddJpegAndPngFilesToTask() throws Exception {
        super.login("trellouitesting@gmail.com");
        driver.findElement(By.linkText("Welcome board")).click();
        driver.findElement(By.cssSelector(".list-card.js-member-droppable")).click();
        driver.findElement(By.cssSelector(".js-attach")).click();
        driver.findElement(By.cssSelector(".js-attachment-url")).sendKeys("https://dl.dropboxusercontent.com/s/026no3x2qw0h8qa/dank-pepe.jpg");
        driver.findElement(By.cssSelector(".js-add-attachment-url")).click();
        Thread.sleep(2000);
        assertEquals("dank-pepe.jpg", driver.findElement(By.cssSelector(".js-attachments-section .js-attachment-name")).getText());


        driver.findElement(By.cssSelector(".js-attach")).click();
        driver.findElement(By.cssSelector(".js-attachment-url")).sendKeys("https://dl.dropboxusercontent.com/s/8li70ragt2krdv4/dank-elf.png");
        driver.findElement(By.cssSelector(".js-add-attachment-url")).click();
        Thread.sleep(2000);
        assertEquals("dank-elf.png", driver.findElement(By.cssSelector(".js-attachments-section .js-attachment-name")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
