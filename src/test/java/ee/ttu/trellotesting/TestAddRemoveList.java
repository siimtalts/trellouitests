package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestAddRemoveList extends TestBase {

    @Test
    public void testAddRemoveList() throws Exception {
        super.login("trellouitesting@gmail.com");

        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a/span[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[7]/form/span")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys("New");
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys("New List");
        driver.findElement(By.xpath("//input[@value='Save']")).click();
        try {
            assertEquals("New List", driver.findElement(By.xpath("//*[@id=\"board\"]/div[7]/div/div[1]/textarea")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a/span[2]")).click();
        try {
            assertEquals("New List", driver.findElement(By.xpath("//*[@id=\"board\"]/div[7]/div/div[1]/textarea")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        testRemoveList();
    }

    private void testRemoveList() throws Exception {
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[7]/div/div[1]/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/div/ul[3]/li/a")).click();
        try {
            driver.findElement(By.xpath("//*[@id=\"board\"]/div[7]/div/div[1]/textarea")).getText();
            verificationErrors.append("Found New list element when it should not be there");
        } catch (Throwable e) {
            //No element anymore. All good.
            LOGGER.info("Element not found");
        }

    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
