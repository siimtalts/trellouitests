package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class TestUserGetsInAppNotification extends TestBase {

    @Test
    public void testUserGetsInAppNotification() throws Exception {
        super.login("trellouitesting@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[1]/div/a[1]")).click();
        driver.findElement(By.xpath("//span[@name='TrelloUiTesting4 (useruitesting4)']")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitestinguser4@gmail.com");

        String count = driver.findElement(By.xpath("//*[@id=\"header\"]/div[4]/a[3]")).getAttribute("data-count");
        if (Integer.decode(count) == 1) {
            LOGGER.info("One notification that is user assigned");
        } else {
            verificationErrors.append("Fail due to precondition not met. Found notifications: ").append(Integer.decode(count));
        }

        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitesting@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[3]/div/a[1]")).click();
        driver.findElement(By.xpath("//div[2]/div/select")).click();
        new Select(driver.findElement(By.xpath("//div[2]/div/select"))).selectByVisibleText("Temp list");
        driver.findElement(By.xpath("//div[2]/div/select")).click();
        driver.findElement(By.xpath("//input[@value='Move']")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitestinguser4@gmail.com");

        count = driver.findElement(By.xpath("//*[@id=\"header\"]/div[4]/a[3]")).getAttribute("data-count");
        if (Integer.decode(count) == 2) {
            LOGGER.info("No notifications");
        } else {
            verificationErrors.append("No notification found after task moved");
        }

        driver.findElement(By.xpath("//*[@id=\"header\"]/div[4]/a[3]/span")).click();
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[3]/div/div[2]/a[4]")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[3]/div/a[1]")).click();
        driver.findElement(By.xpath("//div[2]/div/select")).click();
        new Select(driver.findElement(By.xpath("//div[2]/div/select"))).selectByVisibleText("test");
        driver.findElement(By.xpath("//div[2]/div/select")).click();
        driver.findElement(By.xpath("//div[2]/div[2]/select")).click();
        new Select(driver.findElement(By.xpath("//div[2]/div[2]/select"))).selectByVisibleText("1");
        driver.findElement(By.xpath("//div[2]/div[2]/select")).click();
        driver.findElement(By.xpath("//input[@value='Move']")).click();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
