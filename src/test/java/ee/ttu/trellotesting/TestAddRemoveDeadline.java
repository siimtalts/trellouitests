package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestAddRemoveDeadline extends TestBase {

    @Test
    public void testAddRemoveDeadline() throws Exception {
        super.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();

        driver.findElement(By.xpath("//*[@id=\"board\"]/div[4]/div/div[2]/a")).click();

        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[1]/div/a[4]")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/form/div[1]/div[1]/label/input")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/form/div[1]/div[1]/label/input")).clear();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/form/div[1]/div[1]/label/input")).sendKeys("21/11/2019");

        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/form/div[3]/input")).click();

        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();

        driver.findElement(By.xpath("//*[@id=\"board\"]/div[4]/div/div[2]/a/div[3]")).click();

        String text = driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[5]/div[1]/div[3]/span/span[2]")).getText();

        try {
            assertEquals("21 Nov 2019 at 12:00", text);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        testRemoveDeadline();
    }

    private void testRemoveDeadline() throws Exception {
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[5]/div[1]/div[3]/span/span[2]")).click();

        driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/form/div[3]/button")).click();
        String text = driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[5]/div[1]/div[3]/span/span[2]")).getText();
        try {
            assertEquals("", text);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
