package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestLogout extends TestBase {

    @Test
    public void testLogout() throws Exception {
        super.login("trellouitesting@gmail.com");
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();
        try {
            assertEquals("Thanks for using Trello.", driver.findElement(By.xpath("//h1")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}


