package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestLogin extends TestBase {

    @Test
    public void testLogin() throws Exception {
        driver.get("https://trello.com/");
        driver.findElement(By.linkText("Log In")).click();
        driver.findElement(By.id("user")).click();
        driver.findElement(By.id("user")).clear();
        driver.findElement(By.id("user")).sendKeys("trellouitesting@gmail.com");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("Wyzjx6EmpN");
        driver.findElement(By.id("login")).click();
        try {
            assertEquals("Personal Boards", driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/div/h3")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
