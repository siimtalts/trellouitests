package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestFilters extends TestBase {

    @Test
    public void testSeeTasksAssignedToMe() throws Exception {
        super.login("trellouitesting@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[6]/div[1]/div/a[1]")).click();
        driver.findElement(By.xpath("//span[@name='TrelloUiTesting4 (useruitesting4)']")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div/a")).click();
        driver.findElement(By.xpath("//div[@id='header']/div/a/span")).click();
        driver.findElement(By.xpath("//div[@id='header']/div[4]/a[4]/span/span")).click();
        driver.findElement(By.linkText("Log Out")).click();

        super.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        List<WebElement> optionCount = driver.findElements(By.xpath("//*[@id=\"board\"]/div[2]/div/div[2]/a"));
        try {
            assertEquals(3, optionCount.size());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        //*[@id="content"]/div/div[2]/div/div/div[2]/div/ul/li[2]/a
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/ul/li[2]")).click();
        driver.findElement(By.xpath("//a[@href='https://trello.com/useruitesting4']")).click();

        try {
            assertEquals(1, getVisibleCount("//*[@id=\"board\"]/div[2]/div/div[2]/a"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//*[@id=\"header\"]/div[4]/a[3]/span")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("/html/body/div[5]/div/div[1]/a")).click();
        driver.findElement(By.cssSelector("div.list-card-members.js-list-card-members > div.member.js-member-on-card-menu > span.member-initials")).click();
        driver.findElement(By.linkText("Remove from Card")).click();
    }

    @Test
    public void testSeeTextFilteredTasks() throws Exception {
        super.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        List<WebElement> optionCount = driver.findElements(By.xpath("//*[@id=\"board\"]/div[3]/div/div[2]/a"));
        try {
            assertEquals(3, optionCount.size());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/ul/li[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/input")).click();
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).clear();
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).sendKeys("complex");

        try {
            assertEquals(1, getVisibleCount("//*[@id=\"board\"]/div[3]/div/div[2]/a"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void testSeeLabelFilteredTasks() throws Exception {
        this.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        List<WebElement> optionCount = driver.findElements(By.xpath("//*[@id=\"board\"]/div[3]/div/div[2]/a"));
        try {
            assertEquals(3, optionCount.size());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/ul/li[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/div/ul[1]/li[2]")).click();

        try {
            assertEquals(1, getVisibleCount("//*[@id=\"board\"]/div[3]/div/div[2]/a"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void testSeeDueDateCompleteTasks() throws Exception {
        this.login("trellouitestinguser4@gmail.com");

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/ul/li[1]/a")).click();
        List<WebElement> elements = driver.findElements(By.cssSelector("a[class*=list-card]:not([class*='hide'])"));
        try {
            assertTrue("More than one element", elements.size() > 1);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div/div[2]/div/ul/li[2]")).click();
        Thread.sleep(200);
        driver.findElement(By.xpath("//a[@time='complete' and @class='js-due-filter']")).click();


        elements = driver.findElements(By.cssSelector("a[class*=list-card]:not([class*='hide'])"));

        try {
            assertEquals(1, elements.size());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    private long getVisibleCount(String xPath) {
        return driver.findElements(By.xpath(xPath))
                .stream()
                .filter(elem -> !elem.getAttribute("class").contains("hide"))
                .count();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
