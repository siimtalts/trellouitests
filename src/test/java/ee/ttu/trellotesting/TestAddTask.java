package ee.ttu.trellotesting;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class TestAddTask extends TestBase {

    @Test
    public void testAddTask() throws Exception {
        super.login("trellouitesting@gmail.com");
        driver.findElement(By.xpath("//div[@id='content']/div/div[2]/div/div/div/ul/li/a/span[2]")).click();
        driver.findElement(By.linkText("Add a card…")).click();
        driver.findElement(By.xpath("//div[@id='board']/div/div/div[2]/div/div/div/textarea")).clear();
        driver.findElement(By.xpath("//div[@id='board']/div/div/div[2]/div/div/div/textarea")).sendKeys("TEST123");
        driver.findElement(By.xpath("//input[@value='Add']")).click();
        driver.findElement(By.xpath("//div[@id='board']/div/div/div[2]/a[17]/div[3]")).click();
        try {
            assertEquals("TEST123", driver.findElement(By.cssSelector(".card-detail-title-assist")).getAttribute("innerHTML"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
