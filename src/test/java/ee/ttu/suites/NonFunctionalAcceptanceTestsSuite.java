package ee.ttu.suites;

import ee.ttu.trellotesting.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestDragAndDropTask.class,
        TestAddJpegPngFileToTask.class,
        TestAddRemoveDeadline.class,
        TestAddRemoveImportanceLabelToTask.class,
        TestAddRemoveList.class,
        TestAddRemoveUserToFromTask.class,
        TestAddTask.class,
        TestChangeBoardName.class,
        TestChangeTaskUser.class,
        TestEditDescriptionOfTask.class,

})
public class NonFunctionalAcceptanceTestsSuite {
}
