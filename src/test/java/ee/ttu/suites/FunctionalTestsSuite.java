package ee.ttu.suites;

import ee.ttu.trellotesting.TestAddRemoveDeadline;
import ee.ttu.trellotesting.TestAddRemoveImportanceLabelToTask;
import ee.ttu.trellotesting.TestAddRemoveList;
import ee.ttu.trellotesting.TestAddRemoveUserToBoard;
import ee.ttu.trellotesting.TestAddRemoveUserToFromTask;
import ee.ttu.trellotesting.TestAddTask;
import ee.ttu.trellotesting.TestChangeBoardName;
import ee.ttu.trellotesting.TestChangeTaskUser;
import ee.ttu.trellotesting.TestCreateNewUser;
import ee.ttu.trellotesting.TestEditDescriptionOfTask;
import ee.ttu.trellotesting.TestFilters;
import ee.ttu.trellotesting.TestLogin;
import ee.ttu.trellotesting.TestLogout;
import ee.ttu.trellotesting.TestUserGetsInAppNotification;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        TestCreateNewUser.class,
        TestLogin.class,
        TestLogout.class,
        TestAddRemoveDeadline.class,
        TestAddRemoveImportanceLabelToTask.class,
        TestAddRemoveList.class,
        TestAddRemoveUserToBoard.class,
        TestAddRemoveUserToFromTask.class,
        TestAddTask.class,
        TestChangeBoardName.class,
        TestChangeTaskUser.class,
        TestEditDescriptionOfTask.class,
        TestFilters.class,
        TestUserGetsInAppNotification.class,
})
public class FunctionalTestsSuite {
}
