package ee.ttu.suites;

import ee.ttu.trellotesting.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        TestCreateNewUser.class,
        TestLogin.class,
        TestLogout.class,
        TestAddRemoveDeadline.class,
        TestAddRemoveImportanceLabelToTask.class,
        TestAddRemoveList.class,
        TestAddRemoveUserToBoard.class,
        TestAddRemoveUserToFromTask.class,
        TestAddTask.class,
        TestChangeBoardName.class,
        TestChangeTaskUser.class,
        TestEditDescriptionOfTask.class,
        TestFilters.class,
        TestUserGetsInAppNotification.class,
})
public class RiskBasedTestsSuite {
}
